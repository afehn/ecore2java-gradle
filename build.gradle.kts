import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.2.51"
    `kotlin-dsl`
    id("java-gradle-plugin")
    id("ivy-publish")
}

group = "ecore2java"
version = "0.1.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    val emfVersion = "2.12.0"
    val codegenVersion = "2.11.0"

    implementation("org.eclipse.emf:org.eclipse.emf.common:$emfVersion")
    implementation("org.eclipse.emf:org.eclipse.emf.ecore:$emfVersion")
    implementation("org.eclipse.emf:org.eclipse.emf.ecore.xmi:$emfVersion")
    implementation("org.eclipse.emf:org.eclipse.emf.codegen:$codegenVersion")
    implementation("org.eclipse.emf:org.eclipse.emf.codegen.ecore:$codegenVersion")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

gradlePlugin {
    (plugins) {
        "ecore2java-plugin" {
            id = "ecore2java"
            implementationClass = "ecore2java.Ecore2JavaPlugin"
        }
    }
}

publishing {
    repositories {
        ivy("file:$buildDir/repo")
    }
}
