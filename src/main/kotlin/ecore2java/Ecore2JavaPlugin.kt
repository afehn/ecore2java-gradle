package ecore2java

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.kotlin.dsl.get
import org.gradle.kotlin.dsl.task
import org.gradle.kotlin.dsl.withConvention
import java.io.File

@Suppress("unused")
open class Ecore2JavaPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.pluginManager.apply(JavaPlugin::class.java)


        val extension = project.extensions.create("ecore2java", Ecore2JavaPluginExtension::class.java, project)

        if (extension.genModels.isPresent) {
            project.task("generateEcore") {
                extension.genModels.get().forEach { inputs.file(it) }
                outputs.dir(extension.outputDir)

                doFirst {
                    val generator = Ecore2JavaGenerator(File(extension.outputDir), logger)

                    extension.genModels.get()
                            .map(::File)
                            .forEach(generator::generate)
                }
            }

            project.withConvention(JavaPluginConvention::class) { sourceSets["main"].java.srcDir(extension.outputDir) }
        }
    }
}
