package ecore2java

import org.eclipse.emf.codegen.ecore.generator.Generator
import org.eclipse.emf.codegen.ecore.genmodel.GenModel
import org.eclipse.emf.codegen.ecore.genmodel.GenModelPackage
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenBaseGeneratorAdapter
import org.eclipse.emf.codegen.merge.java.JControlModel
import org.eclipse.emf.common.util.BasicMonitor
import org.eclipse.emf.common.util.Diagnostic
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.gradle.api.logging.Logger
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.PrintStream

class Ecore2JavaGenerator(outputDir: File, logger: Logger) {
    private val resourceSet = ResourceSetImpl().apply {
        uriConverter.uriMap[URI.createURI("platform:/resource/")] = URI.createFileURI(outputDir.absolutePath + File.separator)
    }

    private val generator = object : Generator() {
        override fun getJControlModel() = JControlModel()
    }.apply {
        adapterFactoryDescriptorRegistry.addDescriptor(GenModelPackage.eNS_URI, OnlyCodeGenModelGeneratorAdapterFactory.Descriptor)
    }

    private val monitor = Monitor(logger)

    init {
        GenModelPackage.eINSTANCE

        Resource.Factory.Registry.INSTANCE.extensionToFactoryMap.let {
            it["genmodel"] = XMIResourceFactoryImpl()
            it["ecore"] = XMIResourceFactoryImpl()
        }
    }

    fun generate(genModelFile: File) {
        var oldErr: PrintStream? = System.err
        try {
            System.setErr(PrintStream(ByteArrayOutputStream()))
        } catch (_: Throwable) {
            oldErr = null
        }

        try {
            val genModel = getGenModel(genModelFile)
            generator.input = genModel
            generator.generate(genModel, GenBaseGeneratorAdapter.MODEL_PROJECT_TYPE, monitor)
        } finally {
            if (oldErr != null) {
                System.setErr(oldErr)
            }
        }
    }

    private fun getGenModel(file: File): GenModel {
        val res = resourceSet.getResource(URI.createFileURI(file.absolutePath), true)

        return (res.contents[0] as GenModel).apply {
            modelDirectory = "."
            setCanGenerate(true)
        }
    }


    private class Monitor(private val logger: Logger) : BasicMonitor() {
        override fun beginTask(name: String?, totalWork: Int) {
            if (name != null && name.isNotEmpty()) {
                log(">>> $name")
            }
        }

        override fun setTaskName(name: String?) {
            if (name != null && name.isNotEmpty()) {
                log("<>> $name")
            }
        }

        override fun subTask(name: String?) {
            if (name != null && name.isNotEmpty()) {
                log(">>  $name")
            }
        }

        override fun setBlocked(reason: Diagnostic) {
            super.setBlocked(reason)
            log("#>  " + reason.message)
        }

        override fun clearBlocked() {
            log("=>  " + blockedReason.message)
            super.clearBlocked()
        }

        private fun log(msg: String) {
            logger.info(msg)
        }
    }
}
