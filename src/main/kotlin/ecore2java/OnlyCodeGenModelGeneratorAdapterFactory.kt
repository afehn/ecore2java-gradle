package ecore2java

import org.eclipse.emf.codegen.ecore.generator.GeneratorAdapterFactory
import org.eclipse.emf.codegen.ecore.genmodel.GenModel
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenModelGeneratorAdapter
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenModelGeneratorAdapterFactory
import org.eclipse.emf.common.util.Monitor

class OnlyCodeGenModelGeneratorAdapterFactory : GenModelGeneratorAdapterFactory() {
    object Descriptor : GeneratorAdapterFactory.Descriptor {
        override fun createAdapterFactory() = OnlyCodeGenModelGeneratorAdapterFactory()
    }

    private val onlyCodeGenModelGeneratorAdapter = object : GenModelGeneratorAdapter(this) {
        override fun generateModelBuildProperties(genModel: GenModel?, monitor: Monitor?) = Unit
        override fun generateModelManifest(genModel: GenModel?, monitor: Monitor?) = Unit
        override fun generateModelModule(genModel: GenModel?, monitor: Monitor?) = Unit
        override fun generateModelPluginClass(genModel: GenModel?, monitor: Monitor?) = Unit
        override fun generateModelPluginProperties(genModel: GenModel?, monitor: Monitor?) = Unit
    }


    override fun createGenModelAdapter() = onlyCodeGenModelGeneratorAdapter
}
