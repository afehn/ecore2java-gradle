package ecore2java

import org.gradle.api.Project
import org.gradle.kotlin.dsl.listProperty

open class Ecore2JavaPluginExtension(project: Project) {
    val genModels = project.objects.listProperty<String>()
    var outputDir = "${project.buildDir}/generated-src/ecore/java"

    fun genModel(genModelFile: String) = genModels.add(genModelFile)
}
