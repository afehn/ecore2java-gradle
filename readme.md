This gradle plugin can be used to generate model code from an ecore `*.genmodel`.

Currently simple setups work.
More complex ones (such with multiple referencing genmodels may work but are unsupported).

# Installation

The plugin can be used via `apply plugin: 'ecore2java'` or with the [Plugins DSL](https://docs.gradle.org/current/userguide/plugins.html#sec:plugins_block) (see below).

For this you need to add the repository in your `build.gradle` file:

```groovy
buildscript {
    repositories {
        // ...
        mavenCentral()
        ivy { setUrl 'https://git-st.inf.tu-dresden.de/afehn/ecore2java-gradle/raw/ivy-repo' }
    }

    dependencies {
        // ...
        classpath('ecore2java:gradle-plugin:<version>')
    }
    // ...
}
```

## Plugins DSL

Add a new repository in your `settings.gradle` file:

```groovy
pluginManagement {
    // ...
    repositories {
        // Important! This allows you to use the default plugins.
        gradlePluginPortal()
        ivy { setUrl 'https://git-st.inf.tu-dresden.de/afehn/ecore2java-gradle/raw/ivy-repo' }
    }
    // ...
}
```

Now you can apply the plugin in your `build.gradle` file:

```groovy
plugins {
    // ...
    id 'ecore2java' version '<version>'
    // ...
}
```



# Usage

Execute the model generation in your `build.gradle` file:

```groovy
ecore2java {
    // Defaults to '$buildDir/generated-src/ecore/java'
    outputDir = 'src-gen'
    genModel 'models/model.genmodel'
}
```
